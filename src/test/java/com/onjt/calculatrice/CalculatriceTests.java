package com.onjt.calculatrice;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class CalculatriceTests {

    @Test
    public void testAddition() {
        assertEquals(15, Calculatrice.addition(7,8));
    }
    @Test
    public void testSoustration() {

        assertEquals(10, Calculatrice.soustraction(20, 10),0.0);
        assertEquals(-1, Calculatrice.soustraction(5, 6),0.0);
        assertEquals(-4, Calculatrice.soustraction(-5, -1),0.0);
    }

    @Test
    public void testMultiplication() {

        assertEquals(10, Calculatrice.multiplication(5, 2),0.0);
        assertEquals(10, Calculatrice.multiplication(-5, -2),0.0);
        assertEquals(-10, Calculatrice.multiplication(-5, 2),0.0);

    }

    @Test
    public void testDivision(){
        assertEquals(5, Calculatrice.division(10, 2));
    }

    @Test
    public void testDivionParZero(){
        assertThrows(ArithmeticException.class,()-> Calculatrice.division(10,0));
    }

    @Test
    public void testMoyenne() {

        double[] nombres = {12,12,12};
        assertEquals(12,Calculatrice.moyenne(nombres),0.0);
    }

    @Test
    public void testMoyenneAvecTableauVide(){

        double [] nombres = {};
        assertThrows(ArithmeticException.class,()-> Calculatrice.moyenne(nombres));
    }
}
